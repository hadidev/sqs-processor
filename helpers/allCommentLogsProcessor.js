const { dataFromQueue, deleteMessage } = require('./awsSqs');
const AllCommentLogs = require('../model/allCommentLogs.js');

var isProcess = true;
var records = [];

async function allCommentLogsProcessor(queueUrl) {
    try {
        while (isProcess) {
            var values = await dataFromQueue("AllCommentLogs", queueUrl);

            if (values) {
                values.forEach(element => {
                    const {
                        SenderName,
                        SenderMobileNo,
                        SenderId,
                        Comment,
                        IsPremium,
                        CompetitionId
                    } = element.MessageAttributes;

                    const dataValue = {
                        SenderName: SenderName.StringValue,
                        SenderMobileNo: SenderMobileNo.StringValue,
                        SenderId: SenderId.StringValue,
                        Comment: Comment.StringValue,
                        IsPremium: IsPremium.StringValue,
                        CompetitionId: CompetitionId.StringValue
                    };

                    records.push(dataValue);
                    deleteMessage(element, queueUrl);
                });

                if (records.length > 1000) {
                    insertMongo(records)
                    records = [];
                };
            } else isProcess = false;
        }
        // Mongo insert for less than 1000 record
        if (records.length > 0) insertMongo(records)
    } catch (err) {
        console.log({ err });
    }
}

async function insertMongo(records) {
    try {
        await AllCommentLogs.collection.insertMany(records, { ordered: false })
    } catch (err) {
        if (err.code != 11000) console.log({ err })
    }
}

module.exports = { allCommentLogsProcessor };
