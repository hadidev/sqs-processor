const AWS = require('aws-sdk');

const sqs = new AWS.SQS({
    apiVersion: process.env.AWS_SQS_API_VERSION,
    region: process.env.AWS_REGION,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

const gameLogsReceiveParams = queueUrl => {
    const params = {
        AttributeNames: [
            "MessageId",
            "CreatedAt",
            "MobileNo",
            "EventType",
            "Data",
            "CompetitionId"
        ],
        MaxNumberOfMessages: 10,
        MessageAttributeNames: [
            "All"
        ],
        QueueUrl: queueUrl,
        WaitTimeSeconds: 0
    };

    return params;
}

const allCommentLogsReceiveParams = queueUrl => {
    const params = {
        AttributeNames: [
            "SenderName",
            "SenderMobileNo",
            "SenderId",
            "Comment",
            "IsPremium",
            "CompetitionId"
        ],
        MaxNumberOfMessages: 10,
        MessageAttributeNames: [
            "All"
        ],
        QueueUrl: queueUrl,
        WaitTimeSeconds: 0
    };

    return params;
}

const jokerHistoryLogsReceiveParams = queueUrl => {
    const params = {
        AttributeNames: [
            "MobileNo",
            "CreatedAt",
            "TotalJokerCount",
            "JokerAmount",
            "ActionType",
            "Source",
            "SourceData",
            "CompetitionId",
            "QuestionId",
            "QuestionIndex"
        ],
        MaxNumberOfMessages: 10,
        MessageAttributeNames: [
            "All"
        ],
        QueueUrl: queueUrl,
        WaitTimeSeconds: 0
    };

    return params;
}

const dataFromQueue = async (type, queueUrl) => {
    try {
        let params;
        switch (type) {
            case "GameLogs":
                params = gameLogsReceiveParams(queueUrl);
                break;
            case "AllCommentLogs":
                params = allCommentLogsReceiveParams(queueUrl);
                break;
            case "JokerHistoryLogs":
                params = jokerHistoryLogsReceiveParams(queueUrl);
                break;
            default:
                console.log(`Wrong type: ${type}`);
        }

        const returnedData = await sqs.receiveMessage(params).promise();

        if (returnedData.Messages) return returnedData.Messages;
        else return false;
    } catch (err) { console.log({ err }) }
}

const deleteMessage = (messagesFirst, queueUrl) => {
    const deleteParams = {
        QueueUrl: queueUrl,
        ReceiptHandle: messagesFirst.ReceiptHandle
    };
    sqs.deleteMessage(deleteParams, function (err, data) {
        if (err) console.log("Delete Error", err);
    });
}

module.exports = { dataFromQueue, deleteMessage };