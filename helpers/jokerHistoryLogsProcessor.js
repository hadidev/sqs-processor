const { dataFromQueue, deleteMessage } = require('./awsSqs');
const JokerHistoryLogs = require('../model/jokerHistoryLogs.js');

var isProcess = true;
var records = [];

async function jokerHistoryLogsProcessor(queueUrl) {
    try {
        while (isProcess) {
            var values = await dataFromQueue("JokerHistoryLogs", queueUrl);

            if (values) {
                values.forEach(element => {
                    const {
                        MobileNo,
                        CreatedAt,
                        TotalJokerCount,
                        JokerAmount,
                        ActionType,
                        Source,
                        SourceData,
                        CompetitionId,
                        QuestionId,
                        QuestionIndex
                    } = element.MessageAttributes;

                    const dataValue = {
                        MobileNo: MobileNo.StringValue,
                        CreatedAt: CreatedAt.StringValue ,
                        TotalJokerCount: TotalJokerCount.StringValue ,
                        JokerAmount: JokerAmount.StringValue ,
                        ActionType: ActionType.StringValue ,
                        Source: Source.StringValue ,
                        SourceData: SourceData.StringValue ,
                        CompetitionId: CompetitionId.StringValue ,
                        QuestionId: QuestionId.StringValue ,
                        QuestionIndex: QuestionIndex.StringValue
                    };

                    records.push(dataValue);
                    deleteMessage(element, queueUrl);
                });

                if (records.length > 1000) {
                    insertMongo(records)
                    records = [];
                };
            } else isProcess = false;
        }
        // Mongo insert for less than 1000 record
        if (records.length > 0) insertMongo(records)
    } catch (err) {
        console.log({ err });
    }
}

async function insertMongo(records) {
    try {
        await JokerHistoryLogs.collection.insertMany(records, { ordered: false })
    } catch (err) {
        if (err.code != 11000) console.log({ err })
    }
}

module.exports = { jokerHistoryLogsProcessor };