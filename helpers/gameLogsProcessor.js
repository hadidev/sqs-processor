const { dataFromQueue, deleteMessage } = require('./awsSqs');
const GameLogs = require('../model/gameLogs.js');

var isProcess = true;
var records = [];

async function gameLogsProcessor(queueUrl) {
    try {
        while (isProcess) {
            var values = await dataFromQueue("GameLogs", queueUrl);

            if (values) {
                values.forEach(element => {
                    const {
                        MessageId,
                        CreatedAt,
                        MobileNo,
                        EventType,
                        Data,
                        CompetitionId
                    } = element.MessageAttributes;

                    const dataValue = {
                        MessageId: MessageId.StringValue,
                        CreatedAt: CreatedAt.StringValue,
                        MobileNo: MobileNo.StringValue,
                        EventType: EventType.StringValue,
                        Data: Data.StringValue,
                        CompetitionId: CompetitionId.StringValue
                    };

                    records.push(dataValue);
                    deleteMessage(element, queueUrl);
                });

                if (records.length > 1000) {
                    insertMongo(records)
                    records = [];
                };
            } else isProcess = false;
        }
        // Mongo insert for less than 1000 record
        if (records.length > 0) insertMongo(records)
    } catch (err) {
        console.log({ err });
    }
}

async function insertMongo(records) {
    try {
        await GameLogs.collection.insertMany(records, { ordered: false })
    } catch (err) {
        if (err.code != 11000) console.log({ err })
    }
}

module.exports = { gameLogsProcessor };
