require('dotenv').config()
const mongoose = require('mongoose');
var cron = require('node-cron');

const { gameLogsProcessor } = require('./helpers/gameLogsProcessor');
const { allCommentLogsProcessor } = require('./helpers/allCommentLogsProcessor');
const { jokerHistoryLogsProcessor } = require('./helpers/jokerHistoryLogsProcessor');

cron.schedule('0 3 * * *', () => {
    mongoose.connect(`mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/eventLogs`,
        {
            keepAlive: 1,
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true,
            authSource: "admin",
            auth: {
                user: process.env.MONGO_USER,
                password: process.env.MONGO_PASSWORD
            }
        }).then(
            () => {
                gameLogsProcessor(process.env.AWS_SQS_GAME_LOG_QUEUE_URL);
                allCommentLogsProcessor(process.env.AWS_SQS_ALL_COMMENTS_QUEUE_URL);
                jokerHistoryLogsProcessor(process.env.AWS_SQS_JOKER_HISTORY_QUEUE_URL);
            },
            err => { console.log({ err }) }
        );
});