const mongoose = require('mongoose');

const JokerHistoryLogsSchema = new mongoose.Schema({
  MobileNo: String,
  CreatedAt: Date,
  TotalJokerCount: Number,
  JokerAmount: Number,
  ActionType: Number,
  Source: String,
  SourceData: String,
  CompetitionId: Number,
  QuestionId: String,
  QuestionIndex: Number
});
mongoose.model('JokerHistoryLogs', JokerHistoryLogsSchema);

module.exports = mongoose.model('JokerHistoryLogs');