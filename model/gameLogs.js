const mongoose = require('mongoose');

const GameLogsSchema = new mongoose.Schema({
  MessageId: { type: 'String', required: true, unique: true, index: true },
  MobileNo: String,
  EventType: Number,
  Data: String,
  CreatedAt: Date
});
mongoose.model('GameLogs', GameLogsSchema);

module.exports = mongoose.model('GameLogs');