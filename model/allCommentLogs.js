const mongoose = require('mongoose');  

const AllCommentLogsSchema = new mongoose.Schema({
  SenderName: String,
  SenderMobileNo: String,
  SenderId: String,
  Comment: String,
  IsPremium: String,
  CompetitionId: Number
});
mongoose.model('AllCommentLogs', AllCommentLogsSchema);

module.exports = mongoose.model('AllCommentLogs');